import React from 'react';
import logo from './logo.svg';
import './App.css';
import {DraftEditorEx} from './components/DraftEditorEx';

function App() {
  return (
    <div className="App">
      {/* <Content /> */}
      <br/>
      <br/>
      <DraftEditorEx />
    </div>
  );
}

export default App;
