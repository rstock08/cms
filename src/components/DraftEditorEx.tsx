import React, { Component } from 'react';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { Button } from '@mui/material';


export class DraftEditorEx extends Component {
  state = {
    editorState: EditorState.createEmpty(),
  }

  onEditorStateChange = (editorState: EditorState): void => {
    this.setState({
      editorState,
    });
  };

  onClick = () => {
    console.log('saving the state');
    console.log(this.state.editorState);
    console.log(JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent())));
  }

  

  render() {
    const { editorState } = this.state;
    return (
      <div>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={(editorState) => this.onEditorStateChange(editorState)}
        />
        <textarea
          disabled
          value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
        />
        <Button onClick={this.onClick}>Send me to console log</Button>
      </div>
    );
  }
}